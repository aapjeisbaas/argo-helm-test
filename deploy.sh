#!/bin/bash

PROJECT_NAME="wordpress"

# check oc connection
oc whoami

# exit if connection failed
if [ $? -ne 0 ]; then
    exit 1
fi

CURRENT_PROCTS=$(oc projects -q)
# if project wordpress does not exist, create it
if [[ ! $CURRENT_PROCTS =~ $PROJECT_NAME ]]; then
    oc new-project $PROJECT_NAME
fi

oc project $PROJECT_NAME

# deploy argocd
oc apply -f minimal-argo.yaml

# wait for argocd to be ready
sleep 60

# deploy project only is special, we get a default * * * * * yolo project 
#oc apply -f minimal-project.yaml

# deploy application in argocd
oc apply -f minimal-application.yaml