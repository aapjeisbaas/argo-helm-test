# argo-helm-test

Example repo of an automated deployment of a helm template with argocd in openshift.

A very minimal project repo


### test in K3s

in k3s without the gitops operator we need to deploy argo and the application together in the same namespace.

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl create namespace wordpress
kubectl apply -n argocd -f https://gitlab.com/aapjeisbaas/argo-helm-test/-/raw/main/minimal-application.yaml
```